<?php
/**
 * 图片模型 
 */
namespace Admin\Model;
use Think\Model;

class PictureModel 
{
    /**
     * 删除指定的图片 
     */
    
    public  function unlink(array $options)
    {
        if (empty($options) ||  !is_array($options))
        {
            return false;
        }
        
        return  \Common\Tool\Tool::partten($options);
    }
    
    /**
     * 构造 删除图片条件 
     * @param array $confition 要检测的数据
     * @param array $validata  要检测的建
     * @return array $validata
     */
    public function buildCondition(array $confition, array $validata)
    {
        if (empty($confition) || empty($validata))
        {
            return array();
        }
        self::pz($confition);
        foreach ($validata as $key => &$value)
        {
            if ( !array_key_exists($value, $confition) || empty($confition[$value]['name']) )
            {
                return false;
            }
        }
        return true;
    }
    
    public static function pz(array &$data)
    {
        foreach ($data as $key => &$value)
        {
            if (is_array($value))
            {
               self::pz($value); 
            }
            else if (empty($value))
            {
                unset($data[$key]);
            }
        }
        return $data;
    }
    
}