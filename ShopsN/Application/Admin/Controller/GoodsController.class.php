<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Admin\Model\GoodsClassModel;
use Admin\Model\PictureModel;
use Admin\Model\HotWordsModel;

//商品管理
class GoodsController extends AuthController {
    
	//列表
    public function goods_list(){
    	$goods_class = M('goods_class');
    	$result_class = $goods_class->where('fid = 0 AND type=1')->order('sort_num ASC')->select();
	
    	$this->assign('result_class',$result_class);
    	
    	$m = M('goods');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	if(!empty($_GET['title'])){
    		$where['title'] = array('like','%'.$_GET['title'].'%');
    	}
     	if(!empty($_GET['keyword'])){
    		$where['keyword'] = array('like','%'.$_GET['keyword'].'%');
    	}
    	if(!empty($_GET['class_id'])){
    		$where['class_id'] = array('eq',$_GET['class_id']);
    	}
        if(!empty($_GET['class_fid'])){
            $where['class_fid'] = array('eq',$_GET['class_fid']);
        }

/*		if($_SESSION['aid'] > 1){
			$where['account_id'] = array('eq',$_SESSION['aid']);
		}*/
		if($_GET['home_tuijian'] != null){
            $where['home_tuijian'] = array('eq',$_GET['home_tuijian']);
        }
        if($_GET['rexiao'] != null){
            $where['rexiao'] =  array('eq',$_GET['rexiao']);
        }
        if($_GET['advert'] != null){
            $where['advert'] =  array('eq',$_GET['advert']);
        }
        if($_GET['recommend'] != null){
            $where['recommend'] =  array('eq',$_GET['recommend']);
        }
        if($_GET['shangjia'] != null){
            $where['shangjia'] =  array('eq',$_GET['shangjia']);
        }
		//dump($_SESSION);
		
    	$where['type'] = 1;
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC,id DESC')->page($nowPage.','.PAGE_SIZE)->select();
        //echo $m->getlastsql();
    	$nid = count($result);
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    		$result[$k]['nid'] = $nid--;
    		$class_name = $goods_class->field('class_name,id')->where('id='.$v['class_id'])->find();
    		$result[$k]['class_name'] = $class_name['class_name'];
    	}
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('result',$result);

    	$this->display();
    }

	
	//隐藏或者打开
	public function class_hide(){
		
		$m = M('GoodsClass');
		$_POST['update_time'] = time();
		$result = $m->save($_POST);
		$this->ajaxReturn(1);	//操作成功
		if($result){
			$this->ajaxReturn(1);	//操作成功
		}else{
			$this->ajaxReturn(0);	//操作失败
		}
	}
    
	/***/
	
	public function goods_add(){
		if(!empty($_POST)){
		    C('GOODS_UPLOAD.rootPath', './Uploads/goods/');
			$upload = new \Think\Upload( C('GOODS_UPLOAD') );// 实例化上传类
			//上传文件
			$info = $upload->upload();
			$_POST['pic_url'] = str_replace('.', '', C('GOODS_UPLOAD.rootPath')) . $info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
			foreach ($info as $k=>$v){
				if($k >= 1){
					$arr[] = $info[$k]['savepath'].$info[$k]['savename'];
				}
			}
			$_POST['pic_tuji'] = serialize($arr);
			
			foreach($_POST['guige_title'] as $k=>$v){
				if(!empty($v)){
					$guige_arr[$k]['guige_title'] = $_POST['guige_title'][$k];
					$guige_arr[$k]['guige_price_new'] = $_POST['guige_price_new'][$k];
					$guige_arr[$k]['guige_zhongliang'] = $_POST['guige_zhongliang'][$k];
				}
			}
			$_POST['taocan'] = serialize($guige_arr);	//序列化

			
			if(!$info) {		// 上传错误提示错误信息
				$this->error($upload->getError());
			}else{		// 上传成功
				$m = M('goods');
				$_POST['create_time'] = time();
				$_POST['type'] = 1;
				
				if($m->add($_POST)){
					$this->success('添加成功');
				}else{
					$this->success('添加失败');
				}
			}
		}else{
			$m = M('goods_class');
			$result_class = $m->where('type=1 AND fid=0')->order('sort_num ASC')->select();
			$this->assign('result_class',$result_class);
			
            $shoper = M('shoper');
			$result_shoper = $shoper->where('status = 1')->select();
            $this->assign('result_shoper',$result_shoper);
			
			$this->display();
		}
	}
	
	public function ajaxgoods() {
		$id = I('post.id');
		$info = M('goods_class')->where('fid='.$id)->select();
		if(empty($info)) {
			$this->ajaxReturn(0);
		}
		$this->ajaxReturn($info);
	}
    
	//编辑
	public function goods_edit()
	{
		$goods_class = M('goods_class');
		$result_class = $goods_class->where('type=1 AND fid=0')->order('sort_num ASC')->select();
		$this->assign('result_class',$result_class);
		
		$m = M('goods');
		$where['id'] = $_GET['id'];	//活动ID
		$result = $m->where($where)->find();
		$where2['id'] = $result['class_id'];
		$class_name = $goods_class->where($where2)->find();
		
		$class_fid_name = $goods_class->where('id='.$result['class_fid'])->find();
		$this->assign('class_fid_name',$class_fid_name);

		
		$result['class_name'] = $class_name['class_name'];
		
        $shoper = M('shoper');
		$where_shoper['id'] = $result['shoper_id'];
		$res_shoper =$shoper->where($where_shoper)->find();

		$result['shoper_name'] = $res_shoper['shoper_name'];
        $result['mobile'] = $res_shoper['mobile'];

		$this->assign('result',$result);

		$result_tuji = unserialize($result['pic_tuji']);
		$this->assign('result_tuji',$result_tuji);

		$taocan = unserialize($result['taocan']);
		$this->assign('taocan',$taocan);
		
		$result_shoper = $shoper->where('status = 1')->select();
		$this->assign('result_shoper',$result_shoper);
		
		
		$this->display();
	}
	
	/**
	 * 保存编辑 
	 */
	public function saveEdit()
	{
	   C('GOODS_UPLOAD.rootPath', './Uploads/goods/');
		if(!empty($_POST)){
		    
    		$m = M('goods');
    		$where['id'] = $_POST['id'];	//活动ID
    		$pic_data = $m->field('pic_url,pic_tuji')->where($where)->find();
    		$upload = new \Think\Upload(C('GOODS_UPLOAD'));// 实例化上传类
    		//上传文件
    		$info = $upload->upload();
    		$path = C('GOODS_UPLOAD.rootPath');
    		if (!empty($info) && $info[0]['key'] === 'pic_url')
    		{
    		    $_POST['pic_url'] = str_replace('.', '', $path).$info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
    		}
    		foreach ($info as $k=> &$v){
    			if($v['key'] === 'pic_tuji'){
    				$arr[] = str_replace('.', '',$path).$v['savepath'].$v['savename'];
    			}
    		}
			
			foreach($_POST['guige_title'] as $k=>$v){
				if(!empty($v)){
					$guige_arr[$k]['guige_title'] = $_POST['guige_title'][$k];
					$guige_arr[$k]['guige_price_new'] = $_POST['guige_price_new'][$k];
					$guige_arr[$k]['guige_zhongliang'] = $_POST['guige_zhongliang'][$k];
				}
			}
			$_POST['taocan'] = serialize($guige_arr);	//序列化		
			
			if(empty($_POST['class_fid'])){
				$this->error('一级商品分类不能为空');
			}
			if(empty($_POST['class_id'])){
				$this->error('二级商品分类不能为空');
			}
			
    		$_POST['pic_tuji'] = serialize($arr);
    		if(empty($_POST['pic_url'])) {
    			unset($_POST['pic_url']);
    		}
    		if($_POST['pic_tuji'] == 'N;'){
    			unset($_POST['pic_tuji']);
    		} 
    		$_POST['update_time'] = time();		//更新时间
    		$result = $m->where($where)->save($_POST);
    		if($result){
    		    $unsetObj = new \Admin\Model\PictureModel();
    		    //构造删除图片条件
    		    $is_del = $unsetObj->buildCondition($_FILES, array('pic_url', 'pic_tuji'));
    		    $status = !$is_del ? FALSE : $unsetObj->unlink($pic_data);
    		    
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}
	}

    //删除活动
    public function goods_del(){
    	$where['id'] = $_POST['id'];
    	$m = M('goods');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = '0';	//删除失败
    		$this->ajaxReturn($data);
    	}
    }
    
	//订单列表 - 全部订单
	public function orders_list(){
		$orders_num = I('get.orders_num');
		$where = array();
		if(!empty($orders_num)){
			$where['orders_num'] = $orders_num;
		}
		$mobile = I('get.mobile');
		if(!empty($mobile)){
			$where['mobile'] = $mobile;
		}
		$pay_status = I('get.pay_status');
		if($pay_status == 1){
			$where['pay_status'] = array('eq',1);
		}else{
			$where['pay_status'] = array('eq',0);
		}
		if($pay_status == ''){
			$where['pay_status'] = array('eq',1);
		}
		$where['order_type'] = 0;
		$orders_num = I('get.orders_status');
		if($orders_num != ''){
			$where['orders_status'] = I('get.orders_status');
		}        
        $realname = I('get.realname');
        if(!empty($realname)){
        	$where['realname'] = array('like',"%$realname%");
        }

        //时间查询
        if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
            $where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
        }
        
        $this->assign('pay_status',$pay_status);

		$goods_orders = M('Goods_orders');
		$count = $goods_orders->where($where)->count();
		$Page  = new \Think\Page($count,10);
		$show  = $Page->show();
		$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		
        $this->assign('page',$show);// 赋值分页输出
		$this->assign('res',$res);
		$this->display();
    }

    //订单列表   - 已支付  待发货
    public function wait_fahuo(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['order_type'] = 0;
    	$where['pay_status'] = 1;
    	$where['orders_status'] = 0;
    	
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    	
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
    //订单列表  - 已发货
    public function done_fahuo(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['order_type'] = 0;
    	$where['pay_status'] = 1;
    	$where['orders_status'] = 2;
    	
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    	
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }    
    
    //订单列表  - 已签收
    public function shouhuo_ed(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['order_type'] = 0;
    	$where['pay_status'] = 1;
    	$where['orders_status'] = 3;	//已签收状态
    
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    
    	//时间查询
    	if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
    		$where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
    	}
    
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show); //赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
    //已退货
    public function tuihuo_ok(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['order_type'] = 0;
    	$where['pay_status'] = 1;
    	$where['tuihuo_chuli_status'] = 1;	//已允许退货
    
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    
    	//时间查询
    	if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
    		$where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
    	}
    
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    	 
    	$this->assign('page',$show); //赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }   

    //订单列表 - 已申请退货
    public function shenqing_tuihuo(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['order_type'] = 0;
    	$where['pay_status'] = 1;
    	$where['orders_status'] = 4;	//已退货状态
    	$where['tuihuo_chuli_status'] = 0;
    	 
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    	 
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show); //赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
    //订单列表  - 已完成
    public function done_jifen(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['pay_status'] = 1;
    	$where['orders_status'] = 5;	//已完成状态
    	$where['order_type'] = 0;
    	 
    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    	
    	//时间查询
    	if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
    		$where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
    	}
    	 
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show); //赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
    
	
    //待支付
    public function wait_pay(){
    	$orders_num = I('get.orders_num');
    	$where = array();
    	if(!empty($orders_num)){
    		$where['orders_num'] = $orders_num;
    	}
    	
    	$mobile = I('get.mobile');
    	if(!empty($mobile)){
    		$where['mobile'] = $mobile;
    	}
    	$where['pay_status'] = 0;	//待支付状态
    	$where['order_type'] = 0;

    	$realname = I('get.realname');
    	if(!empty($realname)){
    		$where['realname'] = array('like',"%$realname%");
    	}
    
    	//时间查询
    	if(!empty($_GET['begin_date']) && !empty($_GET['end_date'])){
    		$where['create_time'] = array('between',array(strtotime($_GET['begin_date']." 00:00:00"),strtotime($_GET['end_date']." 23:23:59")));
    	}
    
    	$goods_orders = M('Goods_orders');
    	$count = $goods_orders->where($where)->count();
    	$Page  = new \Think\Page($count,20);
    	$show  = $Page->show();
    	$res = $goods_orders->where($where)->order('id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
    
    	$this->assign('page',$show); //赋值分页输出
    	$this->assign('res',$res);
    	$this->display();
    }
    
	//添加发货信息
    public function kuaidi_add(){
    	if(IS_POST){
    		$info = M('Goods_orders');
    		$data['id'] = I('id');
    		$data['kuaidi_name'] = I('kuaidi_name');
    		$data['kuaidi_num'] = I('kuaidi_num');
    		$data['fahuo_time'] = time();
            $data['orders_status'] = 2;
    		$result = $info->save($data);
    		if(!$result){
    			$this->ajaxReturn(0);
    		}else{
    			$this->ajaxReturn(1);
    		}
    	}else{
    		$result['id'] = $_GET['id'];
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
	

	//订单详情
	public  function order_lst_detail(){
		$id = I('get.id');
		$goods_orders = M('Goods_orders');
		$res = $goods_orders->where('id='.$id)->find();
		$g = M('Goods_orders_record');

		$good_id = $res['id'];
		$res['result'] = $g->where('goods_orders_id='.$good_id)->select();

		//dump($res);
		$this->assign('list',$res);
		$this->assign('jfa',$res['use_jf_limit']);
		$this->assign('jfb',$res['use_jf_currency']);
		$this->display();

	}

    //删除订单
    public function goods_orders_del(){
    	$m = M('goods_orders');
    	$result = $m->where('id='.$_POST['id'])->delete();
    	if($result){
    		$m_record = M('goods_orders_record');
    		$m_record->where('goods_orders_id='.$_POST['id'])->delete();
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }
    
    //订单详情
    public function orders_edit(){
    	if(!empty($_POST)){
    		$m = M('goods_orders');
    		$_POST['update_time'] = time();
    		$where['id'] = $_POST['id'];	//订单ID
    		$result = $m->where($where)->save($_POST);
    		if($result){
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}else{
    		$where['id'] = $_GET['id'];
    		$m = M('goods_orders');
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		
    		//订单对应的商品信息
    		$orders_record = M('goods_orders_record');
    		$where2['orders_id'] = $result['orders_id'];	//订单ID
    		$goods_data = $orders_record->where($where2)->select();
    		$this->assign('goods_data',$goods_data);
    		$this->display();
    	}   	
    }
    
    //发货
    public function orders_fahuo(){
        if(!empty($_POST)){
    		$m = M('goods_orders');    		
    		$where['id'] = $_POST['id'];	//订单ID
    		$_POST['fahuo_time'] = time();	//发货时间
    		$_POST['orders_status'] = 1;	//-1 取消订单状态    0待发货状态      1 已发货状态
    		$result = $m->where($where)->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$m = M('goods_orders');
    		$where['id'] = $_GET['id'];
    		$result = $m->field('id,kuaidi_name,kuaidi_num,fahuo_time')->where($where)->find();
    		$result['fahuo_time'] = empty($result['fahuo_time'])?'':date('Y-m-d H:i:s',$result['fahuo_time']);
    		$this->assign('result',$result);
    		$this->display();
    	}   
    }
    
    //更新排序
    public function goods_sort(){
    	$m = M('goods');
    	$str_id = explode(',', substr($_GET['str_id'],1));
    	$str_sort = explode(',', substr($_GET['str_sort'],1));
    	foreach ($str_id as $k=>$v){
    		$data['sort_num'] = $str_sort[$k];
    		$m->where('id='.$v)->save($data);
    	}
    	$this->ajaxReturn(1);
    }
    
    //商品分类列表
    public function class_list(){
        
    	if(!empty($_POST)){
    	    C('GOODS_UPLOAD.rootPath', './Uploads/class/');
          	$upload = new \Think\Upload(C('GOODS_UPLOAD'));// 实例化上传类
            //上传文件
            $info = $upload->upload();
         
            $path = C('GOODS_UPLOAD.rootPath');
            
            if (!empty($info) && array_key_exists('pic_url', $info))
            {
                $_POST['pic_url'] = str_replace('.', '', $path).$info['pic_url']['savepath'].$info['pic_url']['savename'];	//上传文件的路径
            }
            
            if(!$info) {        // 上传错误提示错误信息
                $this->error($upload->getError());
            }else{
                $result = GoodsClassModel::getInition()->add($_POST);
                $result ?   $this->success('保存成功') : $this->error('保存失败');
            }
    		
    	} else {
    		
    	    $result = GoodsClassModel::getInition()->select(array(
    		    'where'  => array('fid' => 0, 'type' => 1),
    		    'field' => array('id', 'class_name', 'pic_url', 'sort_num', 'is_show_nav'),
    		    'order'  => array('create_time DESC')
    		));
    	    
    		$this->assign('result',$result);
    		$this->display();
    	}    	
    }


    
    //更新排序
    public function class_sort(){
    	$m = M('goods_class');
    	$str_id = explode(',', substr($_GET['str_id'],1));
    	$str_sort = explode(',', substr($_GET['str_sort'],1));
    	foreach ($str_id as $k=>$v){
    		$data['sort_num'] = $str_sort[$k];
    		$m->where('id='.$v)->save($data);
    	}
    	$this->ajaxReturn(1);
    }    
    
    //删除分类
    public function class_del(){
    	$m = M('goods_class');
    	$where['id'] = $_POST['id'];
    	$checkone = M('goods')->where('class_id='.$where['id'])->find();
    	if(!empty($checkone)){
    		$this->ajaxReturn(2);//分类下有商品
    	}else{
    		$result = $m->where($where)->delete();
    		if($result){
    			$this->ajaxReturn(1);	//删除成功
    		}else{
    			$this->ajaxReturn(0);	//删除失败
    		}
    	}
    }
    
    //更新分类
    public function class_update(){
    	$m = M('goods_class');
    	$_POST['update_time'] = time();
    	$result = $m->save($_POST);
    	if($result){
    		$this->ajaxReturn(1);	//删除成功
    	}else{
    		$this->ajaxReturn(0);	//删除失败
    	}
    }
    
    //添加二级分类
    public function goods_addpage(){
    	if(IS_POST){
    		$data = I('post.');
    		$data['fid'] = $_GET['id'];
    		$data['type'] = 1;
    		$goods_class = M('goods_class');
    		if($goods_class->create($data)){
    			if($goods_class->add($data)){
    				$this->success('子类添加成功',U('Goods/goods_addpage'));
    			}else{
    				$this->error('添加失败');
    			}
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$resone = M('goods_class')->where('fid=0')->select();
    		$this->assign('resone',$resone);
    		$this->display();
    	}
    }
    
    //添加颜色属性值
    public function goodscolor_add(){
    	if(IS_POST){
    		$goods_color = M('goods_color');
    		$data = I('post.');
    		$resone = $goods_color->add($data);
    		if($resone){
    			$this->success('颜色添加成功',U('Goods/goodscolor_add'));
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$goods_color = M('goods_color');
    		$result = $goods_color->order('id desc')->select();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //删除颜色
    public function color_del(){
    	$colorid = $_POST['id'];
    	$goods_color = M('goods_color');
    	$resone = $goods_color->where('id='.$colorid)->delete();
    	if($resone){
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }
    
    //更新颜色
    public function color_update(){
    	$colorid = I('post.id');
    	$data['color_name'] = I('post.color_name');
    	$goods_color = M('goods_color');
    	$result = $goods_color->where('id='.$colorid)->save($data);
    	if($result){
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }
    
    //添加规格属性值
    public function goods_size(){
    if(IS_POST){
    		$goods_size = M('goods_size');
    		$data = I('post.');
    		$resone = $goods_size->add($data);
    		if($resone){
    			$this->success('属性名添加成功');
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$result = M('goods_size')->where('fid=0')->order('id asc')->select();
    		foreach($result as $n=>$val){
    			$result[$n]['vo'] = M('goods_size')->where('fid='.$val['id'])->select();
    		}
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //删除属性值
    public function size_del(){
    	$sizeid = I('post.id');
    	$goods_size = M('goods_size');
    	$resone = $goods_size->where('id='.$sizeid)->delete();
    	if($resone){
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }
    
    //更新属性值
    public function size_update(){
    	$sizeid = I('post.id');
    	$data['size_name'] = I('post.size_name');
    	$goods_size = M('goods_size');
    	$result = $goods_size->where('id='.$sizeid)->save($data);
    	if($result){
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }
    
    //添加属性值
    public function goods_sizeval(){
    	$sizeid = $_GET['id'];
    	$goods_size = M('goods_size');
    	if(IS_POST){
    		$data['fid'] = $sizeid;
    		$data['size_name'] = I('post.size_name');
    		$result = $goods_size->add($data);
    		if($result){
    			$this->success('属性值添加成功');
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$find = $goods_size->where('fid=0')->select();
    		$this->assign('find',$find);
    		$this->display();
    	}
    }
    
    //更改属性值
    public function goods_sizevaledit(){
    	$sizeval_id = $_GET['id'];
    	if(IS_POST){
    		$data['fid'] = I('post.fid');
    		$data['size_name'] = I('post.size_name');
    		$goods_size = M('goods_size');
    		$resone = $goods_size->where('id='.$sizeval_id)->save($data);
    		if($resone){
    			$this->success('更新成功');
    		}else{
    			$this->error('更新失败');
    		}
    	}else{
    		$goods_size = M('goods_size');
    		$find = $goods_size->where('fid=0')->select();
    		$findone = $goods_size->where('id='.$sizeval_id)->find();
    		$this->assign('find',$find);
    		$this->assign('findone',$findone);
    		$this->display();
    	}
    }
    
    //添加商品规格
    public function goods_addsize(){
    	$id = $_GET['id'];
    	if(IS_POST){
    		$data['sid'] = I('post.fid');
    		$data['vid'] = I('post.vid');
    		$data['price'] = I('post.price');
    		$data['gid'] = $id;
    		$data['creatime'] = time();
    		$resone = M('goods_sizeval')->add($data);
    		if($resone){
    			$this->success('属性值添加成功');
    		}else{
    			$this->error('添加失败');
    		}
    	}else{
    		$goods_size = M('goods_size');
    		$result = $goods_size->where('fid=0')->select();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    public function goods_sizevaladd(){
    	if(IS_POST){
    		$fid = $_POST['fid'];
    		$resone = array();
    		$goods_size = M('goods_size');
    		$resone = $goods_size->where('fid='.$fid)->select();
    		$this->ajaxReturn($resone,"JSON");
    	}else{
    		$this->error('选择错误');
    	}
    }
    
    //属性管理
    public function goods_shuxing_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['create_time'] = time();
    		$result = $m->add($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$this->display();
    	}
    }
    
    public function goods_shuxing_edit(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //属性列表
    public function goods_shuxing_list(){
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->select();
    	foreach ($result as $k=>$v){
    		$result[$k]['shuxing_content'] = explode('|', $v['shuxing_content']);
    	}
    	$this->assign('result',$result);
    	$this->display();
    }
    
    public function goods_shuxing_del(){
    	$where['id'] = $_POST['id'];
    	$m = M('goods_shuxing');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    	}else{
    		$data['code'] = '0';	//删除失败
    	}
    	$this->ajaxReturn($data);
    }
    
    //商品属性内容添加
    public function goods_shuxing_content_add(){
    	if(!empty($_POST)){
    		$m = M('goods_shuxing');
    		$_POST['update_time'] = time();
    		$_POST['shuxing_content'] = implode('|', $_POST['shuxing_content']);
    		$result = $m->save($_POST);
    		if($result){
    			$this->success('添加成功');
    		}else{
    			$this->error('添加错误');
    		}
    	}else{
    		$m = M('goods_shuxing');
    		$where['id'] = $_GET['id'];
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
	 //关联属性及价格
    public function goods_shuxing_val_price(){
    	if(!empty($_POST)){			
			$arr['create_time'] = time();
			
			$arr['price'] = $_POST['price'];
			$arr['kucun'] = $_POST['kucun'];
			$arr['goods_id'] = $_POST['goods_id'];
			
			unset($_POST['price']);
			unset($_POST['kucun']);
			unset($_POST['goods_id']);
			unset($_POST['button']);

			foreach($_POST as $k=>$v){
				$arr['shuxing_content'] .= '|'.$v;
			}
			$arr['taocan'] = $arr['shuxing_content'].'|';

    		$m = M('goods_content_var_price');
			$result = $m->add($arr);
			if($result){
				$this->success('添加成功');
			}else{
				$this->error('添加成功');
			}
    	}else{
    		$m = M('goods_shuxing');
    		$result = $m->select();
    		foreach ($result as $k=>$v){
    			$result[$k]['shuxing_content'] = explode('|', $v['shuxing_content']);
    		}
    		$this->assign('result',$result);
			
			$m = M('goods_content_var_price');
			$where['goods_id'] = $_GET['goods_id'];
			$result_guige = $m->where($where)->select();
			$this->assign('result_guige',$result_guige);
			
    		$this->display();
    	}

    }
	
    public function goods_val_price_del(){
    	$where['id'] = $_POST['id'];
    	$m = M('goods_content_var_price');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    	}else{
    		$data['code'] = '0';	//删除失败
    	}
    	$this->ajaxReturn($data);
    }
	
	//保存更新
	public function update_val_price(){
		$m = M('goods_content_var_price');
		$_POST['update_time'] = time();
		$result = $m->save($_POST);
		if($result){
			$this->success('修改成功');
		}else{
			$this->error('修改失败');
		}
	}
	
	//一级分类是否首页推荐
	public function shoutui(){
		if(IS_POST){
			$id = $_POST['id'];
			$findone = M('goods_class')->where('id='.$id)->find();
			$count = M('goods_class')->where('shoutui=1')->count();
				if($findone['shoutui'] == 0){
					if($count>9){
						$this->ajaxReturn(3);
					}else{
						$data['shoutui'] = 1;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(1);
					}
				}else if($findone['shoutui'] == 1){
						$data['shoutui'] = 0;
						$resone = M('goods_class')->where('id='.$id)->save($data);
						$this->ajaxReturn(2);
				}
		}
	}
  
    //更换分类图标
    public function edit_class_img()
    {
       
        if(!empty($_POST)) {
           
            C('GOODS_UPLOAD.rootPath', './Uploads/class/');

            $data = GoodsClassModel::getInition()->find(array(
                'where' => array('id' => $_POST['id']),
                'field' => array('pic_url')
            ));
            
            $upload = new \Think\Upload(C('GOODS_UPLOAD'));// 实例化上传类
            //上传文件
            $info = $upload->upload();
         
            $path = C('GOODS_UPLOAD.rootPath');
            
            if (!empty($info) && $info[0]['key'] === 'pic_url')
            {
                $_POST['pic_url'] = str_replace('.', '', $path).$info[0]['savepath'].$info[0]['savename'];	//上传文件的路径
            }
            //添加数据
            $result = GoodsClassModel::getInition()->save($_POST, array(
                'where' => array('id' => $_POST['id']),
            ));
            //删除原来的图片
            if ($result)
            {
                $unlink = new PictureModel();
                $status = $unlink->unlink($data);
                $this->success('更新成功');
            }
            else 
            {
                $this->error('更新失败');
            }
        } else {
            $m = M('goods_class');
            $where['id'] = $_GET['id'];
            $result = $m->where($where)->find();
            $this->assign('result',$result);
            $this->display();
        }
    }
	
    
    //同意退货申请
    public function tuohuo_ok(){
        $m = M('goods_orders');
        $where['id'] = $_GET['id'];     //订单
        $data['tuihuo_status'] = 1;     //拖后时间
        $res = $m->where($where)->save($data);
        if($res){
            $this->success('已同意退货申请');
        }else{
            $this->error('操作错误,请重试');
        }       
    }

    //取消退货申请
    public function tuohuo_no(){
        $m = M('goods_orders');
        $where['id'] = $_GET['id'];     //订单
        $data['tuihuo_status'] = 0;     //拖后时间
        $res = $m->where($where)->save($data);
        if($res){
            $this->success('已取消退货申请');
        }else{
            $this->error('操作错误,请重试');
        }       
    }
	//标记订单为完成状态
	public function done(){
		if(IS_AJAX){
			$order_id=I('order_id');
			$password=I('password');
			$goods_orders=M('goods_orders');
			$pwd=M('pwd')->where(array('id'=>1))->getField('password');
			if($pwd!=md5($password)){
				$this->ajaxReturn(2);//权限密码错误
				exit;
			}
			$account=session('account');
			$biaoji=$goods_orders->where(array('id'=>$order_id))->save(array('orders_status'=>5,'fanli_action'=>$account.'触发，时间：'.date('Y-m-d H:m:s',NOW_TIME)));
			//获取商品的返利积分
			//$jifen=M('goods_orders_record')->where(array('goods_orders_id'=>$order_id))->sum('fanli_jifen');
		    if($biaoji){
				//调用提成逻辑
				R("Shop/buy",array($order_id));
				$this->ajaxReturn(1);
			}else{
				$this->ajaxReturn(0);
			}
		}
	}

    //退货处理
    public function tuihuo_chuli(){
        $m = M('goods_orders');
        $data['tuihuo_chuli_status'] = $_GET['status'];
        $data['tuihuo_chuli_time'] = time();
        $map['id'] = $_GET['orders_id'];

if($_GET['status'] == -1){
        $result_aa = $m->where($map)->find();
        if(empty($result_aa['fahuo_time'])){
            $data['orders_status'] = 0;
        }else{
            $data['orders_status'] = 2;
        }
}

        $result = $m->where($map)->save($data);
        if($result){
            $this->success('操作成功');
        }else{
            $this->error('系统异常,操作失败');
        }
    }
    
    /**
     * 是否显示在导航栏 
     */
    public function is_show_nav()
    {
        /**监测传值*/
        $this->checkPost($_POST, array('is_numeric' => array('is_show_nav', 'id')), true, array('is_show_nav', 'id')) === false ? $this->ajaxReturnData(null, 0, '灌水机制一打开'):true;
        $model = M('goods_class');
        
        $id = $model->save($model->create());
        $status = empty($id) ? 0 : 1;
        
        $message = empty($id) ? '请求失败' : '请求成功';
        
        $this->ajaxReturnData($id, $status, $message);
        
    }
    /**
     * 关键词设置 
     */
    public function hotWords()
    {
        //显示关键词列表
        $data = HotWordsModel::getInition()->getAll( array(
            'order' => array('create_time DESC', 'update_time DESC'),
        ), GoodsClassModel::getInition()) ;
        
        $this->data = $data;
        $this->display();
    }
    
    /**
     * 删除关键词 
     */
    public function deleteHotWords()
    {
        \Common\Tool\Tool::checkPost($_POST, array(
            'is_numeric' => array('id')
        ), true, array('id')) ? true : $this->ajaxReturnData(null, 0, '参数错误');
        
        $isSuccess = HotWordsModel::getInition()->where('id="'.$_POST['id'].'"')->delete();
        $status  = !empty($isSuccess) ? 1 : 0;
        $message = !empty($isSuccess) ? '删除成功' : '删除失败';
        $this->ajaxReturnData(null, $status, $message);
    }
    
    /**
     * 添加关键词 
     */
    public function add_hot_words()
    {
        $this->classData = GoodsClassModel::getInition()->getAllClassId(array(
            'where' =>array(
                'hide_status' => 0,
                'type'        => 1,
            ), 
            'field'=> array(
               'id', 'class_name', 'fid'
            )
        ));
        $this->display();
    }
    
    /**
     * 返回树形结构
     */
    
    public function buildTree()
    {
        $this->ajaxReturnData($this->getClass());
    }
    /**
     * 保存关键词 
     */
    public function add_save_hotwords()
    {
        \Common\Tool\Tool::checkPost($_POST, array(
            'is_numeric' => array('goods_class_id', 'is_hide')
        ), true, array('goods_class_id', 'hot_words', 'is_hide')) ? true : $this->ajaxReturnData(null, 0, '数据有误，请重新输入');
        
        if (HotWordsModel::getInition()->isHaveHotWords($_POST))
        {
            $this->ajaxReturnData(null, 0, '该分类已存在该关键词');
        }
        
        $insert_id = HotWordsModel::getInition()->add($_POST);
        
        $status    = empty($insert_id) ? 0 : 1;
        $message   = empty($insert_id) ? '添加失败' : '添加成功';
        
        $this->ajaxReturnData($insert_id, $status, $message);
    }
    
    /**
     * 编辑 
     */
    public function editHotWords()
    {
        \Common\Tool\Tool::checkPost($_GET, array(
            'is_numeric' => array('id')
        ), true, array('id')) ? true : $this->error('灌水机制已打开');
        
        $data = HotWordsModel::getInition()->find(array(
            'where' => array('id = "'.$_GET['id'].'"'),
            'field' => array('id', 'hot_words', 'goods_class_id', 'is_hide')
        ), GoodsClassModel::getInition());
        //获取商品分类
        $this->classData = $this->getClass();
        $this->data = $data;
        
        $this->display();
    }
    
    /**
     * 保存编辑 
     */
    public function saveHotWords()
    {
        \Common\Tool\Tool::checkPost($_POST, array(
            'is_numeric' => array('goods_class_id', 'is_hide', 'id')
        ), true, array('goods_class_id', 'hot_words', 'is_hide', 'id')) ? true : $this->ajaxReturnData(null, 0, '数据有误，请重新输入');
        
        $insert_id = HotWordsModel::getInition()->save($_POST);
        
        $status    = empty($insert_id) ? 0 : 1;
        $message   = empty($insert_id) ? '更新失败' : '更新成功';
        
        $this->ajaxReturnData($insert_id, $status, $message);
    }
}




