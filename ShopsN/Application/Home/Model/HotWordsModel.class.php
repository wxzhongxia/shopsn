<?php
namespace Home\Model;

use Think\Model;

/**
 * 关键词模型 
 */
class HotWordsModel extends Model
{
    private static $obj;
    
    /**
     * 查询 单个 关键词数据
     */
    public function find($options = array(), Model $model)
    {
        if (empty($options) || !is_array($options) || !($model instanceof Model))
        {
            return array();
        }
        
        $data = parent::find($options);
        
        if (!empty($data))
        {
            $data['children'] = $model->field( $model->getPk() )->where('fid = "'.$data['goods_class_id'].'"')->select();
        }
        
        return $data;
       
    }
    
    /**
     * 处理多级数 
     */
    public function parseData($options=array(), Model $model)
    {
        $data = $this->find($options, $model);
        
        $data = \Common\Tool\Tool::join($data);
       
        return $data;
    }
    
    public static function getInitnation()
    {
       return  self::$obj = !(self::$obj instanceof HotWordsModel) ? new self() : self::$obj;
    }
}