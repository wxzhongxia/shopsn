<?php
namespace Home\Model;

use Think\Model;

/**
 * 用户模型 
 */
class UserModel extends Model
{
    /**
     * 获取 积分余额 
     */
    public function getIntegral($userId)
    {
        if (empty($userId) || !is_numeric($userId))
        {
            return array();
        }
        return $this->where(array('id'=>$userId))->field('add_jf_currency,add_jf_limit')->find();
    }
    
}