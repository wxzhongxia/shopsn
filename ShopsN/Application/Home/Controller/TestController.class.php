<?php
namespace Home\Controller;
use Think\Controller;
use Think\Model;

//前台模块
class TestController extends Controller{
	public function index(){
		$member=M('member','vip_');
		$user_huifei=M('user_huifei');
		$user=M('user');
		$user_review_history=M('user_review_history','vip_');
		if(I('get.s')==1){
			$data=$user_huifei->order('pay_time')->where(array('pay_status'=>1,'hf_money'=>365))->select();
			foreach($data as $k=>$v){
				$my=$member->where(array('id'=>$v['user_id']))->find();
				
				$pid=$my['pid'];
				$p=$member->where(array('id'=>$pid))->find();
				$pname=$p['true_name'];
				if($pname==''){
					$pname=$user->where(array('id'=>$pid))->getField('realname');
				}
				$mobile=$my['mobile'];
				$data[$k]['pid']=$pid;
				$data[$k]['mobile']=$mobile;
				$data[$k]['pname']=$pname;
				if($my['true_name']==''){
					$my['true_name']=$user->where(array('id'=>$my['id']))->getField('realname');
				}
				$data[$k]['true_name']=$my['true_name'];
				unset($my);
				unset($pid);
				unset($p);
				unset($pname);
				
			}
		}else{
			$data=$user_review_history->select();
			foreach($data as $k=>$v){
				$grade=$member->where(array('id'=>$v['user_id']))->getField('grade_name');
				if($grade=='会员'){
					$num=$user_huifei->where(array('pay_status'=>1,'user_id'=>$v['user_id']))->count();
					if($num<=0){
						//echo $v['user_id'];
						//echo '<br/>';
					}
				}
			}
		}
		
		$this->assign('rows',$data);
		$this->display();
	}
	
}