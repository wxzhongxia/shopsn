<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Home\Controller;
use Think\Controller;

//后台管理员
class ShopController extends Controller {

	/**
	 * @param $orders_num   订单号
	 * @return bool
	 */
	public function buy($order_id){
		$order=M('goods_orders','db_');
		$orderMessage=$order->where(array('id'=>$order_id))->find();		
		//$order->startTrans(); 
		
		//dump($orderMessage);
		$myjifen=$orderMessage['fanli_jifen'];
		/**
		if(empty($orderMessage)){
			//echo 0;//不存在此订单
			exit;
		}
		if($orderMessage['is_used']==1){
			//echo 9;//已经被使用
			exit;
		}
		if($orderMessage['price_sum']<=0){
			//echo -1;//支付金额为0或者为负数,不执行逻辑
			exit;
		}
		if($orderMessage['pay_status']!=1){
			//echo 2;//订单未支付
			exit;
		}
		**/
		if($orderMessage['pay_status']==1 && $orderMessage['is_used']!=1 && $orderMessage['price_sum']>0){
			//标记订单号已经用于提成，防止重复调用array('orders_num'=>$orders_num)
			$order->where(array('id'=>$order_id))->save(array('is_used'=>1));
			$year=date('Y',NOW_TIME);
			$month=date('m',NOW_TIME);
			//执行提成逻辑
			$this->makeCompanyData($year,$month);
			$money=$orderMessage['price_sum']+$orderMessage['use_jf_currency']+$orderMessage['use_jf_limit'];
			$member_id=$orderMessage['user_id'];
			$member=M('member','vip_');
			$my=$member->where(array('id'=>$member_id))->find();
			$person=M('person_month','vip_');
			$company=M('company_month','vip_');
			$parent=$member->where(array('id'=>$my['pid']))->find();
			$pparent=$member->where(array('id'=>$parent['pid']))->find();
			$ppparent=$member->where(array('id'=>$pparent['pid']))->find();
			//公司部分先增加购物收益金额
			$company->where(array('year'=>$year,'month'=>$month))->setInc('shop_add',$money);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('add_heji',$money);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('shop_jifen_pay',$myjifen);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen);
			//保存自己的分月记录
			$this->makeMonth($my['id'],$year,$month);
			$person->where(array('member_id'=>$my['id'],'year'=>$year,'month'=>$month))->setInc('my_buy_jf',$myjifen);
			//增加自身购物返利积分
			$this->addUserJf($my['id'],$myjifen);
			if($my['grade_name']=='合伙人'){
				if($parent['id']){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_pay',$money*$bfb);
					if($parent['id']!=10000000 && $parent['id']!=0){
						$company->where(array('year'=>$year,'month'=>$month))->setInc('shop_pay',$money*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
					$this->addMyMoney($parent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('Gross_income'=>$money*$bfb));
				}
				if($pparent['id']){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_pay',$money*$bfb);
					if($pparent['id']!=10000000 && $pparent['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
					$this->addMyMoney($pparent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('Gross_income'=>$money*$bfb));
				}
				if($ppparent['id']){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(6,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_pay',$money*$bfb);
					if($ppparent['id']!=10000000 && $ppparent['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
					$this->addMyMoney($ppparent['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('Gross_income'=>$money*$bfb));
				}
			}elseif($my['grade_name']=='会员'){
				//上级三级之内的会员增加收益
				if($parent['id'] && $parent['grade_name']=="会员" && $this->isVipEnd($parent['id'])){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($parent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($parent['id'],array('add_jf_limit'=>$money*$bfb));
					$this->addUserJf($parent['id'],$money*$bfb);
					if($parent['id']!=10000000 && $parent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$money*$bfb);
					}
				}
				if($pparent['id'] && $pparent['grade_name']=="会员" && $this->isVipEnd($pparent['id'])){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($pparent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($pparent['id'],array('add_jf_limit'=>$money*$bfb));
					$this->addUserJf($pparent['id'],$money*$bfb);
					if($pparent['id']!=10000000 && $pparent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$money*$bfb);
					}
				}
				if($ppparent['id'] && $ppparent['grade_name']=="会员" && $this->isVipEnd($ppparent['id'])){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(2,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_pay',$money*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$money*$bfb/2);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$money*$bfb/2);
					$this->addMyMoney($ppparent['id'],array('add_jf_currency'=>$money*$bfb));
					$this->addMyMoney($ppparent['id'],array('add_jf_limit'=>$money*$bfb));
					$this->addUserJf($ppparent['id'],$money*$bfb);
					if($ppparent['id']!=10000000  && $ppparent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_jifen_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$money*$bfb);
					}
				}
				//最接近的合伙人,向上数三级每个0.1
				$path=explode('-',$my['path']);
				//dump($path);
				array_pop($path);
				array_pop($path);
				$path=array_reverse($path,false);
				//dump($path);
				$least=array();
				foreach($path as $k=>$v){
					$the_row=$member->where(array('id'=>$v))->find();
					if($the_row['grade_name']=="合伙人"){
						$least[]=$the_row;
					}
					if(count($least)>=4){
						break;
					}
				}
				
				//$order->rollback();
				//找到最接近的合伙人,增加0.5
				if($least[0]['id'] && $least[0]['grade_name']=="合伙人"){
					$this->makeMonth($least[0]['id'],$year,$month);
					$bfb=$this->getPercent(array(9,1));
					$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy',$money*$bfb);//
					if($this->isGt($least[0]['id'],$member_id)){
						$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc("hhr_hytd_buy_money",$money);
					}
					//echo $person->getLastSql();exit;
					$this->addMyMoney($least[0]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[0]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[0]['id'],array('Gross_income'=>$money*$bfb));
					if($parent['id']!=10000000 && $parent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
				}
				if($least[1]['id'] && $least[1]['grade_name']=="合伙人"){
					$this->makeMonth($least[1]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,1));
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_money',$money);
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_buy_pay',$money*$bfb);//
					//$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc("hhr_hytd_buy_money",$money);
					$this->addMyMoney($least[1]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[1]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[1]['id'],array('Gross_income'=>$money*$bfb));
					if($least[1]['id']!=10000000 && $least[1]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
				}
				if($least[2]['id'] && $least[2]['grade_name']=="合伙人"){
					$this->makeMonth($least[2]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,2));
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_money',$money);
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_buy_pay',$money*$bfb);
					//$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy_money',$money);
					$this->addMyMoney($least[2]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[2]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[2]['id'],array('Gross_income'=>$money*$bfb));
					if($least[2]['id']!=10000000 && $least[2]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
				}
				if($least[3]['id'] && $least[3]['grade_name']=="合伙人"){
					$this->makeMonth($least[3]['id'],$year,$month);
					$bfb=$this->getPercent(array(6,3));
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_money',$money);
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_buy_pay',$money*$bfb);
					//$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy_money',$money);
					$this->addMyMoney($least[3]['id'],array('surplus_money'=>$money*$bfb));
					$this->addMyMoney($least[3]['id'],array('consume_profit'=>$money*$bfb));
					$this->addMyMoney($least[3]['id'],array('Gross_income'=>$money*$bfb));
					if($least[3]['id']!=10000000 && $least[3]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
					}
				}
				//dump($least);exit;
				//判断三级之内是否存在合伙人
				if($parent['grade_name']=="合伙人" || $pparent['grade_name']=="合伙人" || $ppparent['grade_name']=="合伙人"){
					if($parent['id'] && $parent['grade_name']=="合伙人"){
						$this->makeMonth($parent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,1));
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_money',$money);
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_buy_pay',$money*$bfb);//
						$this->addMyMoney($parent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($parent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($parent['id'],array('Gross_income'=>$money*$bfb));
						if($parent['id']!=10000000 && $parent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
						}
					}
					if($pparent['id'] && $pparent['grade_name']=="合伙人" && $parent['grade_name']=="会员"){
						$this->makeMonth($pparent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,2));
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_money',$money);
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_buy_pay',$money*$bfb);//
						$this->addMyMoney($pparent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($pparent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($pparent['id'],array('Gross_income'=>$money*$bfb));
						if($pparent['id']!=10000000 && $pparent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
						}
					}
					if($ppparent['id'] && $ppparent['grade_name']=="合伙人" && $parent['grade_name']=="会员" && $pparent['grade_name']=="会员"){
						$this->makeMonth($ppparent['id'],$year,$month);
						$bfb=$this->getPercent(array(12,3));
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_money',$money);
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_buy_pay',$money*$bfb);//
						$this->addMyMoney($ppparent['id'],array('surplus_money'=>$money*$bfb));
						$this->addMyMoney($ppparent['id'],array('consume_profit'=>$money*$bfb));
						$this->addMyMoney($ppparent['id'],array('Gross_income'=>$money*$bfb));
						if($ppparent['id']!=10000000 && $ppparent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('shop_pay', $money * $bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('pay_heji',$money*$bfb);
						}
					}
				}
			}else{
				return false;//既不是会员,也不是合伙人的购物
			}
		}
	}

	/**
	 * @param $member_id   用户id
	 * @param array $field  需要增加的字段和增加值的字段 例如:array('jifenA'=>10,'jifenB'=>22)
	 */
	protected function addMyMoney($member_id,$field=array()){
		$member=M('member','vip_');
		//增加账户表
		$member_profit=M('member_profit','vip_');
		if(!$member_profit->where(array('member_id'=>$member_id))->count()){
			$member_profit->add(array('member_id'=>$member_id));
		}
		foreach($field as $k=>$v){
			$member_profit->where(array('member_id'=>$member_id))->setInc($k,$v);
		}
	}
	/**
	*增加自身user表中的积分
	**/
	public function addUserJf($id,$jf){
		$user=M('user','db_');
		$member=M('member','vip_');
		$grade=$member->where(array('id'=>$id))->getField('grade_name');
		if($grade=='合伙人'){
			$user->where(array('id'=>$id))->setInc('add_jf_currency',$jf);
			$user->where(array('id'=>$id))->setInc('integral',$jf);
		}elseif($grade=='会员'){
			$user->where(array('id'=>$id))->setInc('add_jf_currency',$jf/2);
			$user->where(array('id'=>$id))->setInc('add_jf_limit',$jf/2);
			$user->where(array('id'=>$id))->setInc('integral',$jf);
		}
		
		
	}
	/**
	 * 获取提成比例
	 * @param $a提成比例的坐标array(),或者说明文字
	 * @return mixed
	 */
	protected function getPercent($a){
		$model=M('member_proportion','vip_');
		//传入的是汉字说明
		if(is_string($a)){
			if($model->where(array('tcbl_nane'=>$a))->count()){
				return $model->where(array('tcbl_nane'=>$a))->find();
			}else{
				return $model->where(array('proportion_name'=>$a))->find();
			}
		}
		//传入坐标
		if(is_array($a)){
		if($a[1]==1){
			$level="first";
		}elseif($a[1]==2){
			$level="second";
		}else{
			$level="third";
		}
		return $model->where(array('id'=>$a[0]))->getField($level);
	}
	}
	protected function makeCompanyData($year,$month){
		$company=M('company_month','vip_');
		if(!$company->where(array('year'=>$year,'month'=>$month))->count()){
			$company->add(array('year'=>$year,'month'=>$month,'create_time'=>NOW_TIME));
		}
	}
	/**
	 * 旅游提成
	 */
	public function travel($order_id){
		$order=M('goods_orders','db_');
		$orderMessage=$order->where(array('id'=>$order_id))->find();		
		//$order->startTrans(); 
		$myjifen=$orderMessage['fanli_jifen'];
		$member_id=$orderMessage['user_id'];
		$member=M('member','vip_');
		$my=$member->where(array('id'=>$member_id))->find();
		if($orderMessage['pay_status']==1 && $orderMessage['is_used']!=1 && $orderMessage['price_sum']>0){
			//标记订单号已经用于提成，防止重复调用array('orders_num'=>$orders_num)
			$order->where(array('id'=>$order_id))->save(array('is_used'=>1));
			$year=date('Y',NOW_TIME);
			$month=date('m',NOW_TIME);
			$this->makeCompanyData($year,$month);
			//合伙人不能使用积分,会员只能使用B积分账户
			if($my['grade_name']=='合伙人'){
				$money=$orderMessage['price_sum'];
			}else{
				$money=$orderMessage['price_sum']+$orderMessage['use_jf_currency'];
			}
			//执行提成逻辑
			$person=M('person_month','vip_');
			$company=M('company_month','vip_');
			$parent=$member->where(array('id'=>$my['pid']))->find();
			$pparent=$member->where(array('id'=>$parent['pid']))->find();
			$ppparent=$member->where(array('id'=>$pparent['pid']))->find();
			//公司部分先增加旅游收益金额
			$company->where(array('year'=>$year,'month'=>$month))->setInc('tourism_add',$money);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('add_heji',$money);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('tourism_jifen_pay',$myjifen);
			$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen);
			//保存自己的分月记录
			$this->makeMonth($my['id'],$year,$month);
			$person->where(array('member_id'=>$my['id'],'year'=>$year,'month'=>$month))->setInc('my_ly_jf',$myjifen);
			//增加自身旅游返利积分
			$this->addUserJf($my['id'],$myjifen);
			$myjifen*=2;
			if($my['grade_name']=='合伙人'){
				if($parent['id']){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(7,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_pay',$myjifen*$bfb);
					if($parent['id']!=10000000 && $parent['id']!=0){
						$company->where(array('year'=>$year,'month'=>$month))->setInc('tourism_jifen_pay',$myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
					$this->addUserJf($parent['id'],$myjifen*$bfb);
					$this->addMyMoney($parent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
				}
				if($pparent['id']){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(7,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_hy_tra_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_hy_tra_pay',$myjifen*$bfb);
					if($pparent['id']!=10000000 && $pparent['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
					$this->addUserJf($pparent['id'],$myjifen*$bfb);
					$this->addMyMoney($pparent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
				}
				if($ppparent['id']){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(7,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_hy_tra_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_hy_tra_pay',$myjifen*$bfb);
					if($ppparent['id']!=10000000 && $ppparent['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
					$this->addUserJf($ppparent['id'],$myjifen*$bfb);
					$this->addMyMoney($ppparent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
				}
			}elseif($my['grade_name']=='会员'){
				//上级三级之内的会员增加收益
				if($parent['id'] && $parent['grade_name']=="会员" && $this->isVipEnd($parent['id'])){
					$this->makeMonth($parent['id'],$year,$month);
					$bfb=$this->getPercent(array(3,1));
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_money',$money);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_pay',$myjifen*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$myjifen*$bfb/2);
					$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$myjifen*$bfb/2);
					$this->addMyMoney($parent['id'],array('add_jf_currency'=>$myjifen*$bfb));
					$this->addMyMoney($parent['id'],array('add_jf_limit'=>$myjifen*$bfb));
					$this->addUserJf($parent['id'],$myjifen*$bfb);
					if($parent['id']!=10000000 && $parent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				if($pparent['id'] && $pparent['grade_name']=="会员" && $this->isVipEnd($pparent['id'])){
					$this->makeMonth($pparent['id'],$year,$month);
					$bfb=$this->getPercent(array(3,2));
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_money',$money);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_pay',$myjifen*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$myjifen*$bfb/2);
					$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$myjifen*$bfb/2);
					$this->addMyMoney($pparent['id'],array('add_jf_currency'=>$myjifen*$bfb));
					$this->addMyMoney($pparent['id'],array('add_jf_limit'=>$myjifen*$bfb));
					$this->addUserJf($pparent['id'],$myjifen*$bfb);
					if($pparent['id']!=10000000 && $pparent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				if($ppparent['id'] && $ppparent['grade_name']=="会员" && $this->isVipEnd($ppparent['id'])){
					$this->makeMonth($ppparent['id'],$year,$month);
					$bfb=$this->getPercent(array(3,3));
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_money',$money);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_pay',$myjifen*$bfb);//
					//两个积分账户
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_currency',$myjifen*$bfb/2);
					$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('add_jf_limit',$myjifen*$bfb/2);
					$this->addMyMoney($ppparent['id'],array('add_jf_currency'=>$myjifen*$bfb));
					$this->addMyMoney($ppparent['id'],array('add_jf_limit'=>$myjifen*$bfb));
					$this->addUserJf($ppparent['id'],$myjifen*$bfb);
					if($ppparent['id']!=10000000  && $ppparent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				//最接近的合伙人,向上数三级每个0.1
				$path=explode('-',$my['path']);
				//dump($path);
				array_pop($path);
				array_pop($path);
				$path=array_reverse($path,false);
				//dump($path);
				$least=array();
				foreach($path as $k=>$v){
					$the_row=$member->where(array('id'=>$v))->find();
					if($the_row['grade_name']=="合伙人"){
						$least[]=$the_row;
					}
					if(count($least)>=4){
						break;
					}
				}
				
				//$order->rollback();
				//找到最接近的合伙人,增加0.5
				if($least[0]['id'] && $least[0]['grade_name']=="合伙人"){
					$this->makeMonth($least[0]['id'],$year,$month);
					$bfb=$this->getPercent(array(10,1));
					$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_tra',$myjifen*$bfb);//
					$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc("hhr_hytd_tra_fanli",$myjifen);
					if($this->isGt($least[0]['id'],$member_id)){
						$person->where(array('member_id'=>$least[0]['id'],'year'=>$year,'month'=>$month))->setInc("hhr_hytd_tra_money",$money);
					}
					//echo $person->getLastSql();exit;
					$this->addMyMoney($least[0]['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
					if($parent['id']!=10000000 && $parent['id']!=0) {
						//公司支出部分
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				if($least[1]['id'] && $least[1]['grade_name']=="合伙人"){
					$this->makeMonth($least[1]['id'],$year,$month);
					$bfb=$this->getPercent(array(7,1));
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_money',$money);
					$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr1_hy_tra_pay',$myjifen*$bfb);//
					//$person->where(array('member_id'=>$least[1]['id'],'year'=>$year,'month'=>$month))->setInc("hhr_hytd_buy_money",$money);
					$this->addMyMoney($least[1]['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
					if($least[1]['id']!=10000000 && $least[1]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				if($least[2]['id'] && $least[2]['grade_name']=="合伙人"){
					$this->makeMonth($least[2]['id'],$year,$month);
					$bfb=$this->getPercent(array(7,2));
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_hy_tra_money',$money);
					$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr2_hy_tra_pay',$myjifen*$bfb);
					//$person->where(array('member_id'=>$least[2]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy_money',$money);
					$this->addMyMoney($least[2]['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
					if($least[2]['id']!=10000000 && $least[2]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				if($least[3]['id'] && $least[3]['grade_name']=="合伙人"){
					$this->makeMonth($least[3]['id'],$year,$month);
					$bfb=$this->getPercent(array(7,3));
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_hy_tra_money',$money);
					$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('sxhhr3_hy_tra_pay',$myjifen*$bfb);
					//$person->where(array('member_id'=>$least[3]['id'],'year'=>$year,'month'=>$month))->setInc('hhr_hytd_buy_money',$money);
					$this->addMyMoney($least[3]['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
					if($least[3]['id']!=10000000 && $least[3]['id']!=0) {
						$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
						$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
					}
				}
				//dump($least);exit;
				//判断三级之内是否存在合伙人
				if($parent['grade_name']=="合伙人" || $pparent['grade_name']=="合伙人" || $ppparent['grade_name']=="合伙人"){
					if($parent['id'] && $parent['grade_name']=="合伙人"){
						$this->makeMonth($parent['id'],$year,$month);
						$bfb=$this->getPercent(array(13,1));
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_money',$money);
						$person->where(array('member_id'=>$parent['id'],'year'=>$year,'month'=>$month))->setInc('hy1_tra_pay',$myjifen*$bfb);//
						$this->addMyMoney($parent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
						if($parent['id']!=10000000 && $parent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay',$myjifen*$bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
						}
					}
					if($pparent['id'] && $pparent['grade_name']=="合伙人" && $parent['grade_name']=="会员"){
						$this->makeMonth($pparent['id'],$year,$month);
						$bfb=$this->getPercent(array(13,2));
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_money',$money);
						$person->where(array('member_id'=>$pparent['id'],'year'=>$year,'month'=>$month))->setInc('hy2_tra_pay',$myjifen*$bfb);//
						$this->addMyMoney($pparent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
						if($pparent['id']!=10000000 && $pparent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
						}
					}
					if($ppparent['id'] && $ppparent['grade_name']=="合伙人" && $parent['grade_name']=="会员" && $pparent['grade_name']=="会员"){
						$this->makeMonth($ppparent['id'],$year,$month);
						$bfb=$this->getPercent(array(13,3));
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_money',$money);
						$person->where(array('member_id'=>$ppparent['id'],'year'=>$year,'month'=>$month))->setInc('hy3_tra_pay',$myjifen*$bfb);//
						$this->addMyMoney($ppparent['id'],array('tour_jifen_profit'=>$myjifen*$bfb));
						if($ppparent['id']!=10000000 && $ppparent['id']!=0) {
							//公司支出部分
							$company->where(array('year' => $year, 'month' => $month))->setInc('tourism_jifen_pay', $myjifen*$bfb);
							$company->where(array('year'=>$year,'month'=>$month))->setInc('jifen_heji',$myjifen*$bfb);
						}
					}
				}
			}else{
				//游客不能旅游
			}
		}
	}

	/**
	 * @param $member_id  用户id
	 * @return int  1未到期,0到期
	 * 如果是合伙人则不需要判断
	 */
	protected function isVipEnd($member_id){
		$endtime=M('member','vip_')->where(array('id'=>$member_id))->getField('vip_end');
		if($endtime-NOW_TIME>0){
			return 1;
		}else{
			return 0;
		}
	}
	/**
	 * @param int $id
	 * @param int $year
	 * @param int $month
	 * 判断用户月份记录是否存在,如果没有就新增一条
	 */
	protected function makeMonth($id,$year,$month) {
		if ($id) {
			$m = M('person_month','vip_');
			$arr=array('member_id' => $id, 'year' => $year, 'month' => $month,'create_time'=>NOW_TIME);
			for($i=1;$i<=8;++$i){
				for($k=1;$k<=3;++$k){
					$arr['per_'.$i.'_'.$k]=$this->getPercent(array($i,$k));
				}
			}
			$arr['per_9']=$this->getPercent(array(9,1));
			$arr['per_10']=$this->getPercent(array(10,1));
			$arr['per_11']=$this->getPercent(array(11,1));
			for($i=12;$i<=13;++$i){
				for($k=1;$k<=3;++$k){
					$arr['per_'.$i.'_'.$k]=$this->getPercent(array($i,$k));
				}
			}
			if (!$m -> where(array('member_id' => $id, 'year' => $year, 'month' => $month)) -> count()) {
				$m -> add($arr);
			}
		}
	}
	/**
	*是否是4级和4级外的会员
	**/
	public function isGt($id1,$id2){
		$member=M('member','vip_');
		$id2_path=$member->where(array('id'=>$id2))->getField('path');
		
		$pos=strpos($id2_path,(string)$id1);
		if(!$pos){
			//echo 1;
			return false;//非下级关系
		}
		if(!$this->vip2vip($id1,$id2)){
			//echo 2;
			return false;//非直辖关系
		}
		$arr=explode("-",$id2_path);
        array_pop($arr);
        $pos2=array_search($id2,$arr);
		$pos1=array_search($id1,$arr);	
		if($pos2-$pos1<=3){
			//echo 3;
			return false;//非4级之外关系
		}
		//echo 4;
		return true;
	}
/**
     * 判断两个id之间是否存在合伙人
     */
    public function vip2vip($id1,$id2){
        if($id1>$id2){
            $temp=$id1;
            $id1=$id2;
            $id2=$temp;
        }
        $m=M('member','vip_');
        $max=$m->where(array('id'=>$id2))->find();
        $arr=explode("-",$max['path']);
        $arr=array_reverse($arr);
        array_pop($arr);
        array_pop($arr);
        foreach($arr as $k=>$v){
            if($id1==$v){
                return true;
            }
            if($m->where(array('id'=>$v))->getField('grade_name')=='合伙人'){
                return false;
            }
        }
        return true;
    }
}




