<?php
namespace Mobile\Controller;
use Think\Controller;

//账号激活控制器
class OtherController extends Controller{
	/* 
	 * 我的订单 
	 */
	public function index(){
		$pwd=I('get.pwd');
		$account=I('get.account');
		$this->assign('account',$account);
		$this->assign('pwd',$pwd);
		$this->display();
	}
	public function jihuo(){
		if(IS_AJAX){
			$mobile=I('post.mobile');
			$account=I('post.account');
			$name=I('post.name');
			$card=I('post.card');
			$pwd=I('post.pwd');
			$password=I('post.password');
			$user=M('user');
			$member=M('member','vip_');
			$admin=M('admin','vip_');
			$mobile=str_replace(' ','',$mobile);
			$account=str_replace(' ','',$account);
			if(substr($account,1,1)!=9){
				$this->ajaxReturn(44);
				exit;
			}
			if($user->where(array('mobile'=>$mobile))->count()){
				$this->ajaxReturn(11);
				exit;
			}
			if(!$user->where(array('mobile'=>$account,'password'=>md5($pwd)))->count()){
				$this->ajaxReturn(22);
				exit;
			}
			$aid=$admin->where(array('account'=>$account))->getField('id');
			$user->where(array('mobile'=>$account))->save(array('mobile'=>$mobile,'password'=>md5($password)));
			$admin->where(array('account'=>$account))->save(array('account'=>$mobile,'password'=>md5($password)));
			$map=array('mobile'=>$mobile,'vip_end'=>NOW_TIME+31622400);
			$map['true_name']=$name;
			$map['card_id']=$card;
			$map['password']=md5($password);
			$member->where(array('mobile'=>$account))->save($map);
			$auth_group_access=M('auth_group_access','vip_');
			$rst=$auth_group_access->add(array('uid'=>$aid,'group_id'=>51));
			if($rst){
				$this->ajaxReturn(33);
				exit;
			}else{
				$this->ajaxReturn(0);
				exit;
			}
			
		}
	}
}