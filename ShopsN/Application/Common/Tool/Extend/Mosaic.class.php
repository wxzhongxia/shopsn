<?php
namespace Common\Tool\Extend;
use Common\Tool\Tool;

/**
 * 拼接工具类 
 */
class Mosaic extends Tool
{
    /**
     * 实现拼接 【适用于一位数组拼接】后续完善
     */
    public function MosaicPath(array $data, $path=null)
    {
        if (empty($data))
        {
            return $data;
        }
        $defaultPath = C('IMG_ROOT_PATH');
        $path = empty($path) ? $defaultPath :$path;
        foreach ($data as $key => &$value)
        {
            $value = false === strpos($value, $defaultPath) ? $path.$value : $value;
        }
        return $data;
    }
    
    /**
     * 拼接字符 
     */
    public function join(array $array, $parseKey = 'children', $join = 'goods_class_id')
    {
        if (empty($array))
        {
            return array();
        }
        $receive = array();
        
        if (array_key_exists($parseKey, $array))
        {
             $receive       = !empty($receive) ? array() : array();
             
             $array[$join]  .=  ','.implode(',', (array)$this->strrtrArray($array[$parseKey], $receive));
            
             unset($array[$parseKey]);
        }
        else 
        {
            foreach ($array as $key => &$value)
            {
                if (array_key_exists($parseKey, $value) && is_array($value))
                {
                   $receive = !empty($receive) ? array() : array();
                   $value[$parseKey] = implode(',', (array)$this->strrtrArray($value[$parseKey], $receive));
                   $value[$join] .= ','.$value[$parseKey];
                   unset($value[$parseKey]);
                }
            }
        }
        return $array;
    }
    
    public function strrtrArray(array $data, array & $receive)
    {
        if (empty($data))
        {
            return array();
        }
        
        foreach ($data as $key => $value)
        {
            if (is_array($value))
            {
                $this->strrtrArray($value, $receive);
            }
            else 
            {
                $receive[] = $value;
            }
        }
        return $receive;
    }
    

    //短信发送
    public function requestSms($url, array $data)
    {
        if (empty(func_get_args()))
        {
            return false;
        }
        $data = http_build_query($data);
        $opts = array (
            'http' => array (
                'method' => 'POST',
                'header'=> "Content-type: application/x-www-form-urlencoded" .
                "Content-Length: " . strlen($data) . "",
                'content' => $data ,
            ),
        );
        $context = stream_context_create($opts);
        $html = file_get_contents($url, false, $context);
        $num = substr($html,0,1);
        
        return $num ==0 ? true : false;
    }
}