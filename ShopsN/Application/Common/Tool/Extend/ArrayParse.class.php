<?php
namespace Common\Tool\Extend;

use Common\Tool\Tool;

/**
 * 数组操作类 
 */
class ArrayParse extends Tool implements \ArrayAccess
{
    private  $array = array();
    protected $children = array();
    
    protected $parseChildren = array();
    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetExists()
     */
    
    public function __construct(array $array)
    {
        $this->array = $array;
    }
    
    public function __set($name, $value)
    {
        if (!isset($this->array[$name]))
        {
            $this->array[$name] = $value;
        }
    }
    
    public function __get($name = null)
    {
        return isset($this->array[$name]) ? $this->array[$name] : $this->array;
    }
    
    public function offsetExists($offset)
    {
        // TODO Auto-generated method stub
        return isset($this->array[$offset]);
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset)
    {
        // TODO Auto-generated method stub
        return $this->array[$offset];
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value)
    {
        // TODO Auto-generated method stub
        $this->array[$offset] = $value;
    }

    /**
     * {@inheritDoc}
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset)
    {
        // TODO Auto-generated method stub
        unset($this->array[$index]);
    }
    
    /**
     * 组合数据 
     */
    public function buildData(array $data = null)
    {
        $data  = empty($data) ? $this->array : $data;
        
        //阵列变量
        extract($data);
        foreach ($children as $key => &$value)
        {
            
            foreach ($configValue as $config => $nameValue)
            {
                if (array_key_exists($value['type_name'], $nameValue))
                {
                    $value['value'] = $nameValue[$value['type_name']];
                }
            }
        }
        
        foreach ($pData as $key => &$value)
        {
            if ($value['p_id'] == 0)
            {
                continue;
            }
            foreach ($children as $type => $name)
            {
                if ($value['id'] == $name['config_class_id'])
                {
                    $value['type_name'] = $name['type_name'];
                    $value['show_type'] = $name['show_type'];
                    $value['type']      = $name['type'];
                    $value['value']     = $name['value'];
                    unset($children[$type]);
                }
            }
        }
        return $pData;
    }
    
    public function buildConfig(array $data = null)
    {
        $data  = empty($data) ? $this->array : $data;
        //阵列变量
        extract($data);
        
        foreach ($children as $key => &$value)
        {
            foreach ($configValue as $config => $nameValue)
            {
                if (array_key_exists($value['type_name'], $nameValue))
                {
                    $value['value'] = $nameValue[$value['type_name']];
                }
            }
        }
        $this->children = $children;
        return $this;
    }
    
    public function parseConfig()
    {
        if (empty($this->children))
        {
            return array();
        }
        $data = $this->children;
        foreach ($data as $key => &$value)
        {
            $value[$value['type_name']] = $value['value'];
            
            unset($data[$key]['value'], $data[$key]['type_name']);
        }
        $this->parseChildren = $data;
        return $this;
    }
    
    public function oneArray( array & $receive, array $data = null)
    {
        $data =  (empty($data)) ? $this->parseChildren : $data;
            
        foreach ($data as $key => $value)
        {
            if (is_array($value))
            {
                $this->oneArray($receive, $value);
            }
            else
            {
                $receive[$key] = $value;
            }
        }
        return $receive;
    }
}