<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 * @如果需要公共控制器，就不要继承AuthController，直接继承Controller
 */
namespace Common\Controller;
use Think\Controller;
use Admin\Model\GoodsClassModel;

//权限认证
class AuthController extends Controller {
	protected function _initialize(){
		//session不存在时，不允许直接访问
		if(!session('aid')){
			$this->error('还没有登录，正在跳转到登录页',U('Public/login'));
		}

		//session存在时，不需要验证的权限
		$not_check = array('Index/index','Index/main',
				'Index/clear_cache','Index/edit_pwd','Index/logout');
		
		//当前操作的请求                 模块名/方法名
		if(in_array(CONTROLLER_NAME.'/'.ACTION_NAME, $not_check)){
			return true;
		}
		
		//下面代码动态判断权限
		//$auth = new Auth();
		//if(!$auth->check(CONTROLLER_NAME.'/'.ACTION_NAME,session('aid')) && session('aid') != 1){
		//	$this->error('没有权限');
		//}
	}
	
	/**
	 * 检测 post 数据
	 * @return bool
	 */
	protected  function checkPost(array &$post, array $notCheck = array('is_numeric' => array()), $isCheckNumber = false, array $validate = null)
	{
	    if (empty($post)) return false;
	    static $flag = 0;
	    foreach ($post as $key => &$value)
	    {
	        if (is_array($value))
	        {
	            $this->checkPost($value, $notCheck, $isCheckNumber);
	        }
	        else
	        {
	            if (!empty($validate) && !in_array($key, $validate))
	            {
	                return false;
	            }
	            if ($isCheckNumber === true && !is_numeric($value) && in_array($key, $notCheck['is_numeric'])) return false;
	            if (in_array($key, $notCheck)){continue; $flag++;}
	            if (!in_array($key, $notCheck) && empty($value))
	            {
	                if ($value === 0 || $value === '0') {continue; $flag++;}
	                else
	                    return false;
	            }
	            else  {$value = addslashes(strip_tags($value));$flag++;}
	        }
	    }
	    if ($flag === 0) {
	        return false;
	    } else {
	        return true;
	    }
	}
	/**
	 * 获取分类 
	 */
	protected  function getClass()
	{
	    if (!S('classData'))
	    {
	        //获取商品分类
            $classData =  GoodsClassModel::getInition()->getChildren(array(
                'hide_status' => 0,
                'fid'         => 0,
                'type'        => 1,
            ), array('id', 'class_name', 'fid'));
        
	        S('classData', $classData, 10);
	    }
	    return  S('classData');
	}
	/**
	 * ajax 返回数据 
	 */
    protected function ajaxReturnData($data, $status= 1, $message = '操作成功')
    {
        $this->ajaxReturn(array(
            'status'  => $status,
            'message' => $message,
            'data'    => $data
        ));
        die();
    }
    
    protected function updateClient($insert_id)
    {
        $status    = empty($insert_id) ? 0 : 1;
        $message   = empty($insert_id) ? '更新失败' : '更新成功';
        $this->ajaxReturnData($insert_id, $status, $message);
    }
    
    protected function addClient($insert_id)
    {
        $status    = empty($insert_id) ? 0 : 1;
        $message   = empty($insert_id) ? '添加失败' : '添加成功';
        $this->ajaxReturnData($insert_id, $status, $message);
    }
}