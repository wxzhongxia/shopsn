/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : zhang618

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-10-31 18:19:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_config_class`
-- ----------------------------
DROP TABLE IF EXISTS `db_config_class`;
CREATE TABLE `db_config_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_class_name` varchar(50) DEFAULT NULL COMMENT '分类配置名称',
  `p_id` int(11) DEFAULT '0' COMMENT '父级分类',
  `is_open` smallint(6) DEFAULT '0' COMMENT '0=》启用配置， 1弃用配置',
  `create_time` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_config_class
-- ----------------------------
INSERT INTO `db_config_class` VALUES ('2', '用户账号', '1', '0', '1477619683', '1477907306');
