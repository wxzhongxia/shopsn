/**
 * 购物车 js
 */

$(document).ready(function(){
	
	$("#tab_1").mouseover(function(){
		$("#tab1").css("display","block");
		$("#tab2").css("display","none");
		$("#tab_1").attr("style","color:#54AA5C; border-bottom:2px solid #54AA5C;");
		$("#tab_2").attr("style","");
	});
	$("#tab_2").mouseover(function(){
		$("#tab1").css("display","none");
		$("#tab2").css("display","block");
		$("#tab_1").attr("style","");
		$("#tab_2").attr("style","color:#54AA5C; border-bottom:2px solid #54AA5C;");
	});
	//单选框操作
	 $('.goods_num').change(function(){
	        var goods_num=  $(this).val();
	        var id = $(this).parent().siblings('.id').val();
	        var price_new =$('#price_new'+id).val();
	        var count = price_new * goods_num;
	        count = count.toFixed(2);
	        $.get("{:U('Cart/update_goods_num')}", { id:id,goods_num:goods_num },function(data){
	            if(data == 1){
	                $('#count'+id).html(count);
	                good_sum();
	            }
	        });

	    })
	//全选框操作
    $('#selectAll').click(function(){
        console.log($('#selectAll').is(':checked'));
        if($('#selectAll').is(':checked')==true){
           // alert(1);
            $('.goods_select').attr('checked',true);
        }else{
           // alert(2);
            $('.goods_select').attr('checked',false);
        }
        good_sum();
    });
	  //增加数量
    $('.add_goods_num').click(function(){
    	
    	var number = parseInt($(this).siblings('input[type="text"]').val())+1;
    	//重置数量
    	if (number > 99 ) {
    		alert('商品数量不能大于99');
    		return false;
    	} else {
    		$(this).siblings('input[type="text"]').val(number);
    	}
    	
    	var is_success = price_alone($(this).attr('aggId'),number);
    	if (is_success) {
    		//小计金额
        	var small_monery = number * $(this).attr('goods_price');
        	$(this).parent().siblings('.jisuan').text(small_monery);
    	} else {
    		alert('库存不足');
    		return false;
    	}
    	if ($(this).parents('.cart_list').find('input[type="checkbox"]').attr('checked')) {
    		//重新计算总金额
        	var monery = $(this).parents('.Order-form').siblings('.div_jiesuan').find('.klhjkhkhj').text();
        	monery = parseInt(monery.replace('￥', '')) + parseInt($(this).attr('goods_price'));
        	$(this).parents('.Order-form').siblings('.div_jiesuan').find('.good_money_sum').text('￥'+monery);
    	}
    	return true;
    });
    
    //递减数量
    $('.drop_goods_num').click(function(){
    	
    	var number = parseInt($(this).siblings('input[type="text"]').val())-1;
    	//重置数量
    	if (number < 1 ) {
    		alert('商品数量不能小于1');
    		return false;
    	} else {
    		$(this).siblings('input[type="text"]').val(number);
    	}
    	
    	var is_success = price_alone($(this).attr('aggId'),number);
    	if (is_success) {
    		//小计金额
        	var small_monery = number * $(this).attr('goods_price');
        	$(this).parent().siblings('.jisuan').text(small_monery);
    	} else {
    		alert('库存不足');
    		return false;
    	}
    	
    	if ($(this).parents('.cart_list').find('input[type="checkbox"]').attr('checked')) {
    		//重新计算总金额
        	var monery = $(this).parents('.Order-form').siblings('.div_jiesuan').find('.klhjkhkhj').text();
        	monery = parseInt(monery.replace('￥', '')) -parseInt($(this).attr('goods_price'));
        	$(this).parents('.Order-form').siblings('.div_jiesuan').find('.good_money_sum').text('￥'+monery);
    	}
    	return true;
    });
    
  //复选框初始化
    var isChecked = true;
    $('.cart_list').each(function(){
    	
    	if(!isChecked) {
    		$(this).find('input[type="hidden"]').each(function(){
    			$(this).attr('disabled','disabled');
    		})
    	}
    	// 商品数量操作
    	$(this).find('.prarent_nub').find('input[type="text"]').attr('disabled','disabled');
    });
    
    /**
     * 复选框操作 （未选中的移除数据）
     */
    $('input[type="checkbox"]').click(function(){
    	if( $(this).attr('checked') ) {
    		$(this).siblings('input[type="hidden"]').each(function(){
    			$(this).removeAttr('disabled');
    			$(this).siblings('.prarent_nub').find('input[type="text"]').removeAttr('disabled');
    		});
    		
    		$(this).parent().siblings('.cart_list').each(function(){
    			if (!$(this).find('input[type="checkbox"]').attr('checked'))
    			{
    				$(this).find('input[type="hidden"]').each(function(){
    					$(this).attr('disabled','disabled');
    				})
    				$(this).siblings('.prarent_nub').find('input[type="text"]').attr('disabled','disabled');
    			}
    			
    		});
    	} else {
    		//disabled
    		$(this).parent().siblings('.cart_list').each(function(){
    			$(this).find('input[type="hidden"]').each(function(){
    				$(this).attr('disabled','disabled');
    			})
    			$(this).siblings('.prarent_nub').find('input[type="text"]').attr('disabled','disabled');
    		});
    	}
    });
});


//计算单个商品的数量
function price_alone(id,nmb){
	var cart_data = null;
    $.ajax({
    	url  : cart_update,
    	type : 'get',
    	data : { id:id,goods_num:nmb },
    	dataTYpe : 'json',
    	async: false,
    	success:function(data) {
    		if(data == 1){
            	cart_data = data;
               return true;
            } else {
               return false;
            }
    	},
    	error : function(res) {
    		alert('请求超时');
    		return false ;
    	}
    });
    return cart_data === null ? false : (cart_data == 1 ? true : false);
}

//删除其中一个商品
function del_good(id){
  var r=confirm("确定要删除吗？")
    if (r==true)
    {
        $.get(cart_del, { id:id },function(data){
            if(data == 1){
                alert('删除成功');
                $('#li'+id).remove();
                good_sum();
            }else {
              alert('删除失败');
            }
        });
    }

}
//计算单个商品时价格
function goods_alone(id){
    //判断是否是全选
    good_sum();
}
//计算多个商品的合集
function good_sum(){
    var str = 0;
    var nub = 0;
    $('.goods_select:checked').each(function(){
         nub += parseInt($(this).siblings('.prarent_nub').find('.goods_num').val());

        str += parseFloat($(this).siblings('.jisuan').text());
    });
    str = str.toFixed(2);
    $('.b_nub').html(nub);
    $('#price_sum').val(str);
    $('.good_money_sum').text(str);
}


//删除选中的商品 Tourism
function del_all(){
    var r=confirm("确定要删除吗？")
    if (r==true){
        var str = '';
        $('.goods_select:checked').each(function(){
            str +=$(this).next('.id').val()+',';
        })
        str = str.substring(0,str.length-1);
        $.post(cart_m,{id:str},function(data){
            if(data.code==1){
                $('.goods_select:checked').each(function(){
                    $(this).parent().remove();
                });
                good_sum();
            }else{
                alert('删除失败');
            }
        },"json");
    }

}


function shoucang(goods_id){
	$.post(shou_cang,{goods_id:goods_id},function(data){		
		if(data == true){
			alert('收藏成功');
			$('#shoucang_'+goods_id).html('已收藏');
		}else{
			alert('已收藏');
		}
	},"json");
}

//检测复选框是否选中
function check()
{
	var i = 0;
	$('input[type="checkbox"]').each(function(){
		if ($(this).is(":checked"))
		{
			i++;
		}
	});
	return i===0 ? false : true;
}